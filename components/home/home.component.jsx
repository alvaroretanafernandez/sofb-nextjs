import React, {Component} from 'react';
import SofbSocialCompoent from '../sofb-social/sofb-social.component';
import './home.component.scss';
import {SOFB_STATIC} from '../../constants/sofb.constants';

class HomeComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { staticContent: SOFB_STATIC.content.home };

    }

    render() {
        return (
            <div className="home text-center">
                <img src={'static/img/' + this.state.staticContent.header} alt="SOFB Landing" className="img-fluid landing"/>
                <div className="claim">
                    <div className="d-flex justify-content-center mb-3">
                        <h1 data-title="Stoked On Fixed Bikes">{this.state.staticContent.h1}</h1>
                        <br/>
                    </div>
                    <div className="row">
                        <div className="col-md-6 col-md-offset-3">
                            <h2>{this.state.staticContent.h2}</h2>
                            <h3 dangerouslySetInnerHTML={{ __html: this.state.staticContent.p1 }}></h3>
                            <h3 dangerouslySetInnerHTML={{ __html: this.state.staticContent.p2 }}></h3>
                            <h3 dangerouslySetInnerHTML={{ __html: this.state.staticContent.p3 }}></h3>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-4 col-md-offset-4 pt-4 text-center">
                            <SofbSocialCompoent size={5}></SofbSocialCompoent>
                        </div>
                    </div>
                </div>
            </div>
        );

    }
}
export default HomeComponent;