import React, {Component} from 'react';
import {SOFB_STATIC} from '../../constants/sofb.constants';
import './sofb-social.component.scss';

class SofbSocialCompoent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            social: SOFB_STATIC.content.global.social
        }
    }

    render() {
        return (
            <div>
                <div className="list-inline">
                    {this.state.social.map(it =>
                        < div className="social" key={it.icon}>
                            <a href={it.link} target="_blank" rel="noopener noreferrer">
                                <i className={'fa fa-'+ it.icon + ' fa-fw fa-'+this.props.size+'x'}></i>
                            </a>
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default SofbSocialCompoent;
