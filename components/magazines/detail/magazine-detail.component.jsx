import React, {Component} from 'react';
import './magazine-detail.component.scss';

class SofbMagazineDetailComponent extends Component {
    render() {
        return (
            <div className="container-fluid">
                <div className="row sofb-subheader">
                    <div className="col-12 p-2">
                        <h2>Magazine {this.props.magazine.id}</h2>
                    </div>
                </div>
                { this.props.magazine!==undefined ?
                    <div className="row justify-content-center p-3">
                        <div className="col-12 col-sm-8 p-4">
                            <div className="col-12 p-2 mb-3">
                                <h1 className="pb-1">
                                     <span className="pull-right">
                                        {this.props.magazine.id > 1 ?
                                            <a className="pag" href={'../magazines/'+ this.props.previous}>
                                                <i className="fa fa-chevron-left"></i>
                                            </a>
                                        : null}
                                        <span className="current">&nbsp;{this.props.magazine.id}&nbsp;</span>
                                         {this.props.magazine.id < 22 ?
                                             <a className="pag" href={'../magazines/'+ this.props.next}>
                                                 <i className="fa fa-chevron-right"></i>
                                             </a>
                                         : null}
                                     </span>
                                </h1>
                            </div>

                            <div className="magazine-item mt-4" style={{width:'100%'}}>
                                <iframe title={'SOFB ISSUE MAG'} className="sofb-iframe" width="100%" height="600px" src={this.props.magazine.frame}/>
                            </div>
                        </div>

                        <div className="col-12 col-sm-4 p-4">
                            <div className="widget search">
                                <h3><i className="fa fa-calendar"></i> Published </h3>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <ul className="arrow">
                                            <li>{this.props.magazine.published}</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="widget categories">
                                <h3><i className="fa fa-list"></i> Summary</h3>
                                <div className="row">
                                    <div className="col-sm-6">
                                        <ul className="arrow">
                                            <li>Hot Dates</li>
                                            <li>Sick Rider</li>
                                            <li>In Straps</li>
                                            <li>Bike Check</li>
                                            <li>News</li>
                                            <li>Interview</li>
                                            <li>Hot Dates</li>
                                            <li>Products</li>
                                            <li>Other Side Of The Lens</li>
                                            <li>Competitions</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="widget tags">
                                <h3><i className="fa fa-tag"></i> Tags</h3>
                                <ul className="tag-cloud">
                                    <li><span className="sofb-tags">Stoked On Fixed Bikes</span></li>
                                    <li><span className="sofb-tags">Fixed</span></li>
                                    <li><span className="sofb-tags">Gear</span></li>
                                    <li><span className="sofb-tags">Bikes</span></li>
                                        <li><span className="sofb-tags">Ride</span></li>
                                        <li><span className="sofb-tags">SOFB</span></li>
                                        <li><span className="sofb-tags">Magazine</span></li>
                                        <li><span className="sofb-tags">Fixie</span></li>
                                        <li><span className="sofb-tags">Fixed Gear</span></li>
                                            <li><span className="sofb-tags">Straps</span></li>
                                            <li><span className="sofb-tags">Handlebars</span></li>
                                            <li><span className="sofb-tags">Bicycle</span></li>
                                            <li><span className="sofb-tags">Alley Cat</span></li>
                                            <li><span className="sofb-tags">Handlebars</span></li>
                                            <li><span className="sofb-tags">Pedal</span></li>
                                            <li><span className="sofb-tags">Issuu</span></li>
                                            <li><span className="sofb-tags">Free Magazine</span></li>
                                            <li><span className="sofb-tags">Online</span></li>
                                            <li><span className="sofb-tags">Track Bikes</span></li>
                                            <li><span className="sofb-tags">FGFS</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                 : null}
            </div>

        );
    }
}
export default SofbMagazineDetailComponent;
