import React, {Component} from 'react';
import './magazines.component.scss';
import Link from 'next/link'


class SofbMagazinesComponent extends Component {
    render() {
        const { magazines } = this.props;
         const magazinesArray = this.props.magazines
            ? this.props.magazines
            : [];


        return (
            <div className="container-fluid sofb-magazines">
                <div className="row sofb-subheader">
                    <div className="col-lg-12 p-2">
                        <h2>Magazines</h2>
                    </div>
                </div>
                <div className="row justify-content-center p-3">
                    <div className="col-lg-12 wrapper-items">
                        <div className="wrap-magazine-items">
                            {
                                magazinesArray && magazinesArray.length > 0 ? magazinesArray.map(it =>
                                <div className="magazine-item  col-6 col-md-4 col-lg-2" key={it.id}>
                                    <div className="item-inner">
                                        <img src={it.img} alt="Magazine SOFB"/>
                                            <div className="overlay" >
                                            <Link   href={'/magazines/'+ it.id}>
                                            <span className="preview btn btn-outline-dark ">
                                                Read</span>
                                            </Link>
                                    </div>
                                </div>
                                </div>
                            ) : null}
            </div>
        </div>
    </div>
</div>
        );
    }
}

export default SofbMagazinesComponent
