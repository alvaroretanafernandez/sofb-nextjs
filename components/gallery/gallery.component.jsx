import React, {Component} from 'react';
import {SOFB_STATIC} from '../../constants/sofb.constants';
import './gallery.component.scss';
import {Carousel} from 'react-bootstrap';

class GalleryComponent extends Component {

    render() {
        const gallerySlides = SOFB_STATIC.content.gallery.body.slides ;
        return (
            <div className="container-fluid">
                <div className="row sofb-subheader">
                    <div className="col-12 p-2">
                        <h2>Gallery</h2>
                    </div>
                </div>
                <div className="row justify-content-center p-3">
                    <div className="col-12">
                        <div className="carousel-sofb">
                        <Carousel interval={5000}>
                            {gallerySlides.map(it =>
                                <Carousel.Item key={it.img}><img src={it.img} alt="Gallery SOFB slide"/></Carousel.Item>)}
                        </Carousel>
                        </div></div>
    </div>
</div>
        );

    }
}
export default GalleryComponent;