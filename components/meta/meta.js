import Head from 'next/head'

export default () => (
    <div>
        <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta charSet="utf-8" />
            <title>Stoked On Fixed Bikes - SOFB - Fixed Gear Bikes Free Online Magazine - Read Fixed Gear News - Read Magazine</title>
            <meta property="og:image" content="./assets/img/og.jpg"/>
            <meta property="og:description" content="Fixie - London - Magazine - News - SOFB - Fixed Gear Bikes - Free Online Magazine"/>
            <meta property="og:title" content="Stoked On Fixed Bikes Free Online Magazine"/>
            <meta name="author" content="freepowder - aretanafernandez@gmail.com"/>
            <link rel="shortcut icon" href="static/favicon.ico"/>
                <link rel="icon" type="image/x-icon" href="static/favicon.ico"/>
            <meta name="description" content="Fixed Gear Bikes Free Online Magazine - Stoked On Fixed Bikes"/>
            <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" async/>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
                  integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
                  crossOrigin="anonymous" async/>
            <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
                  integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
                  crossOrigin="anonymous" async />
        </Head>

        <style jsx global>{`
           html, body{
              height: 100%;
              width:100%;
              font-family: Roboto,sans-serif;
              font-size: 81.25%;
            }
    `}</style>
    </div>
)