import React, {Component} from 'react';
import './videos.component.scss'

class SofbVideosComponent extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row sofb-subheader">
                        <div className="col-12 p-2">
                            <h2>Videos</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center p-4">
                        <div className="col-lg-12">
                            <div className="portfolio-items">
                                {this.props.videos.length > 0 ? this.props.videos.map(it =>
                                    <li key={it._id} className="portfolio-item  col-md-2 col-xs-6" onClick={() => this.handleClick(it)}>
                                        <div className="item-inner">
                                            <img src={it.preview} alt={it.title}/>
                                            <p className="title">
                                                <span className="text-muted pull-left">{it.title}</span>
                                            </p>
                                        </div>
                                    </li>
                                ) : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default SofbVideosComponent;
