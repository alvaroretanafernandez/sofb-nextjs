import React, {Component} from 'react';
import {SOFB_STATIC} from '../../constants/sofb.constants';
import './advertising.component.scss';
class AdvertComponent extends Component {

    render() {

        const state = { header: SOFB_STATIC.content.advertising.header, body: SOFB_STATIC.content.advertising.body }

        return (
            <div className="container-fluid">
                <div className="row sofb-subheader">
                    <div className="col-12 p-2">
                        <h2>Advertising</h2>
                    </div>
                </div>
                <div className="row justify-content-center p-4">
                    {state.header.sections.map(it =>
                        <div key={it.icon} className="col-xs-12 col-md-3 col-sm-3 p-4">
                            <div>
                                <span className="fa-stack fa-4x">
                                <i className="fa fa-circle fa-stack-2x grey"></i>
                                <i className={'fa ' +it.icon+ ' fa-stack-1x white'}></i>
                                </span>
                                <h2><span> { it.title}</span></h2>
                                <h4 className="text-muted" dangerouslySetInnerHTML={{ __html: it.desc }}></h4>
                            </div>
                        </div>
                    )}
                        <div className="col-lg-12 p-4 text-center">
                            <h2> {state.header.title}</h2>
                        </div>
                </div>
                <div className="col-lg-12 wrapper-items text-center">
                    <div className="row justify-content-center" >
                        {state.body.oldAdverts.map(it =>
                            <div className="col-md-1" key={it.img}>
                                <span className="thumbnail">
                                    <img className="img-fluid" alt="Old SOFB advert" src={it.img}/>
                                </span>
                            </div>
                        )}
                    </div>
                </div>

            </div>
        );
    }
}


export default AdvertComponent;
