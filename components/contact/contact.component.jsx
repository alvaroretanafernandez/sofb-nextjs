import React, {Component} from 'react';
import './contact.component.scss'
class ContactComponent extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid contact">
                    <div className="row sofb-subheader">
                        <div className="col-12 p-2">
                            <h2>Contact</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center p-4">
                        <div className="col-lg-12 p-3 text-center">
                            <h2>
                                The best way to contact the SOFB team is by dropping us an email with your enquiry to
                                any of this email
                                addresses.
                            </h2>
                            <div className="row hidden-xs">
                                <div className="col-lg-12 p-4">
                                    <button type="button"
                                            className="btn btn-lg btn-outline-dark">greg@sofbonlinemag.com
                                    </button>
                                </div>
                                <div className="col-12 p-4">
                                    <button type="button"
                                            className="btn btn-lg btn-outline-dark">stokedonfixedbikes@gmail.com
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center p-1">
                        <div className="col-lg-12">
                            <iframe title={'SOFB maps'}
                                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d21826378.853786834!2d4.1763366315610995!3d48.10342795476175!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46ed8886cfadda85%3A0x72ef99e6b3fcf079!2sEurope!5e0!3m2!1sen!2sus!4v1445781869188"
                                width="100%" height="350" frameBorder="0" allowFullScreen></iframe>
                        </div>
                    </div>

                </div>
                <div className="row">
                    <img src={'https://s3.eu-central-1.amazonaws.com/sofbheaders/8.jpg'} className=" img-fluid banner" alt=" SOFB banner"/>
                </div>
            </div>
        );
    }
}

export default ContactComponent;
