import Meta from '../meta/meta'
import NavbarWithLinks from './sofb-navbar/sofb-navbar.component'
import './index.scss'
import SofbFooterComponent from './sofb-footer/sofb-footer.component';
import SofbBannerComponent from './sofb-banner/sofb-banner.component';
import SofbPrefooterComponent from './sofb-prefooter/sofb-prefooter.component';
export default ({ children }) => (
    <div>
        <Meta/>
        <NavbarWithLinks/>
        { children }

        <SofbBannerComponent/>
        <SofbPrefooterComponent/>
        <SofbFooterComponent/>

    </div>

)


