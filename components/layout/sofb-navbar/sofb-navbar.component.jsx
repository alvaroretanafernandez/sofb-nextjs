
import React, { Component } from 'react';
import { Nav, Navbar, NavItem } from 'react-bootstrap';
import './sofb-navbar.scss';
import { withRouter } from 'next/router'

import {SOFB_STATIC} from '../../../constants/sofb.constants';


const NavbarWithLinks = ({ children, router }) => {
    const  menu =  SOFB_STATIC.content.global.menu ;
    return (
        <header>
        <Navbar inverse fluid>
            <Navbar.Header>
                <Navbar.Brand>
                    <a href="/">
                        <img src="../../static/img/sofb-mini.png" className="logo" alt="Stoked On Fixed Bikes Logo"/>
                    </a>
                </Navbar.Brand>
            </Navbar.Header>
            <Nav pullRight>
                {menu.map(it =>

                      <NavItem key={it.state} href={it.state} >
                          {it.title}
                      </NavItem>


                )}
            </Nav>
        </Navbar>
        </header>
    )
}

export default withRouter(NavbarWithLinks)