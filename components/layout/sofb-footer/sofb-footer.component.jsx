import React, {Component} from 'react';
import {SOFB_STATIC} from '../../../constants/sofb.constants';
import './sofb-footer.component.scss';
class SofbFooterComponent extends Component {
    render() {
        const menu = SOFB_STATIC.content.global.menu
        return (
            <footer className="container-fluid">
            <div className="row">
                <div className="col-md-8 col-md-offset-2">
                    <ul className="list-inline-footer">
                        <li>
                            {menu.map(it => <a href={it.state} key={it.state}>{it.title}</a>)}
                        </li>
                     </ul>
                </div>
            </div>
            <div className="row">
                <div className="col end-fot">
                    <a target=" _blank" href="/" className=" text-muted"
                       title=" powered by ARF">freepowder </a> &copy; <span className=" text-muted">2018 </span>
                    <a target=" _blank" href="/" className=" text-muted"
                       title=" Stoked On Fixed Bikes">StokedOnFixedBikes</a>
                </div>
            </div>
         </footer>
        );
    }
}


export default SofbFooterComponent;
