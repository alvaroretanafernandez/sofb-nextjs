import React, {Component} from 'react';
import {SOFB_STATIC} from '../../../constants/sofb.constants';
import SofbSocialCompoent from '../../sofb-social/sofb-social.component';
import './sofb-prefooter.component.scss';

class SofbPrefooterComponent extends Component {
    render() {
        const state = {
            staticContent: SOFB_STATIC.content.global.prefooter.content,
            social:  SOFB_STATIC.content.global.social
        }
        return (
            <div className="container-fluid prefoot-wrap">
                <div className="row">
                    {state.staticContent.map((it, i) =>
                        <div className="col-12 col-sm-4" key={i}>
                            <h2>{it.title}</h2>
                            <h4 className="p-1" dangerouslySetInnerHTML={{__html: it.text}}></h4>
                            {i === 0 ? <SofbSocialCompoent size={4}></SofbSocialCompoent> : null}
                            {i === 1 ?  <a href="/magazines/22">
                                            <p className="p-2">
                                                <img src={it.image} width="140px" alt="SOFB Latest magazine"/>
                                            </p>
                                        </a>: null
                            }
                            {i === 2 ?  <a href="/">
                                <p className="p-2">
                                    <img src={it.image} width="250px" alt="SOFB Logo"/>
                                </p>
                            </a>: null
                            }
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default SofbPrefooterComponent;
