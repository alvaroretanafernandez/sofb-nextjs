import React, {Component} from 'react';
import {SOFB_STATIC} from '../../constants/sofb.constants';
import './about.component.scss';

class AboutComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { staticContent: SOFB_STATIC.content.about };

    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row sofb-subheader">
                    <div className="col-12 p-2">
                        <h2>About</h2>
                    </div>
                </div>
                <div className="row justify-content-center p-4">
                    <div className="col-md-12 text-center">
                        <h2>
                            { this.state.staticContent.header.h2}
                        </h2>
                        <h3 className=" text-muted">
                            { this.state.staticContent.header.h3}
                        </h3>
                        <p className="lead">
                            { this.state.staticContent.header.p}
                        </p>
                    </div>
                </div>
                <div className="row justify-content-center p-4">
                    <div className="col-md-12 text-center">
                        <h2>
                            { this.state.staticContent.body.h2}
                        </h2>
                        <h3 className=" text-muted">
                            { this.state.staticContent.body.p}
                        </h3>
                    </div>
                    {this.state.staticContent.body.members.map(item =>
                        <div className="col-md-4 col-xs-6" key={item.name}>
                            <div className="text-center">
                                <p><img className="img-fluid img-circle avatar" src={item.avatar} alt="Avatar SOFB member"/></p>
                                <h5>{item.name}
                                    <small className="designation muted"> &nbsp;{item.role}</small>
                                </h5>
                                <p>{item.desc}</p>
                            </div>
                        </div>
                    )}
                </div>
            </div>
        );

    }
}
export default AboutComponent;