import React, {Component} from 'react';
import './links.component.scss'
import SofbLinkItemComponent from './components/link-item.component';

class SofbLinksComponent extends Component {
    render() {
        return (
            <div>
                <div className="container-fluid">
                    <div className="row sofb-subheader">
                        <div className="col-12 p-2">
                            <h2>Links</h2>
                        </div>
                    </div>
                    <div className="row justify-content-center p-4">
                        <div className="col-lg-12">
                            <div className="links-wrap">
                                {this.props.links.length > 0 ? this.props.links.map(it =>
                                    <div className="col-md-2" key={it.id}>
                                        <SofbLinkItemComponent item={it}></SofbLinkItemComponent>
                                    </div>
                                ) : null}

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default SofbLinksComponent;
