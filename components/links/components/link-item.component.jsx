import React, {Component} from 'react';
import './link-item.component.scss'

class SofbLinkItemComponent extends Component {
    render() {
        return (
            <div>
                <a href={this.props.item.url}
                   rel="noopener noreferrer"
                   target="_blank"
                   title={this.props.item.title}
                   className="thumbnail">
                    <img src={this.props.item.img} alt={'stoked on fixed bikes -' + this.props.item.title} className="thumbnail-img"/>
                </a>
            </div>
        );
    }
}

export default SofbLinkItemComponent;