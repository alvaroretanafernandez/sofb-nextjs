webpackHotUpdate("static/development/pages/magazines.js",{

/***/ "./components/magazines/magazines.component.jsx":
/*!******************************************************!*\
  !*** ./components/magazines/magazines.component.jsx ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _magazines_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./magazines.component.scss */ "./components/magazines/magazines.component.scss");
/* harmony import */ var _magazines_component_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_magazines_component_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/link */ "./node_modules/next/link.js");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var SofbMagazinesComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(SofbMagazinesComponent, _Component);

  function SofbMagazinesComponent() {
    _classCallCheck(this, SofbMagazinesComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(SofbMagazinesComponent).apply(this, arguments));
  }

  _createClass(SofbMagazinesComponent, [{
    key: "render",
    value: function render() {
      var magazines = this.props.magazines;
      var magazinesArray = this.props.magazines ? this.props.magazines : [];
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid sofb-magazines"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row sofb-subheader"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-lg-12 p-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "Magazines"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row justify-content-center p-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-lg-12 wrapper-items"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "wrap-magazine-items"
      }, magazinesArray && magazinesArray.length > 0 ? magazinesArray.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "magazine-item  col-6 col-md-4 col-lg-2",
          key: it.id
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "item-inner"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "Magazine SOFB"
        }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "overlay"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_2___default.a, {
          href: '/magazines/' + it.id
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
          className: "preview btn btn-outline-dark "
        }, "Read")))));
      }) : null))));
    }
  }]);

  return SofbMagazinesComponent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SofbMagazinesComponent);

/***/ })

})
//# sourceMappingURL=magazines.js.5e2d593800f8af38b163.hot-update.js.map