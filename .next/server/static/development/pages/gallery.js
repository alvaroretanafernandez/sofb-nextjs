module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 7);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/gallery/gallery.component.jsx":
/*!**************************************************!*\
  !*** ./components/gallery/gallery.component.jsx ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constants/sofb.constants */ "./constants/sofb.constants.js");
/* harmony import */ var _gallery_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./gallery.component.scss */ "./components/gallery/gallery.component.scss");
/* harmony import */ var _gallery_component_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_gallery_component_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var GalleryComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(GalleryComponent, _Component);

  function GalleryComponent() {
    _classCallCheck(this, GalleryComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(GalleryComponent).apply(this, arguments));
  }

  _createClass(GalleryComponent, [{
    key: "render",
    value: function render() {
      var gallerySlides = _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__["SOFB_STATIC"].content.gallery.body.slides;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row sofb-subheader"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-12 p-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, "Gallery"))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row justify-content-center p-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-12"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "carousel-sofb"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Carousel"], {
        interval: 5000
      }, gallerySlides.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_3__["Carousel"].Item, {
          key: it.img
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "Gallery SOFB slide"
        }));
      }))))));
    }
  }]);

  return GalleryComponent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (GalleryComponent);

/***/ }),

/***/ "./components/gallery/gallery.component.scss":
/*!***************************************************!*\
  !*** ./components/gallery/gallery.component.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/layout/index.scss":
/*!**************************************!*\
  !*** ./components/layout/index.scss ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/layout/main.layout.js":
/*!******************************************!*\
  !*** ./components/layout/main.layout.js ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _meta_meta__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../meta/meta */ "./components/meta/meta.js");
/* harmony import */ var _sofb_navbar_sofb_navbar_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sofb-navbar/sofb-navbar.component */ "./components/layout/sofb-navbar/sofb-navbar.component.jsx");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./index.scss */ "./components/layout/index.scss");
/* harmony import */ var _index_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_index_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _sofb_footer_sofb_footer_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./sofb-footer/sofb-footer.component */ "./components/layout/sofb-footer/sofb-footer.component.jsx");
/* harmony import */ var _sofb_banner_sofb_banner_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./sofb-banner/sofb-banner.component */ "./components/layout/sofb-banner/sofb-banner.component.jsx");
/* harmony import */ var _sofb_prefooter_sofb_prefooter_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./sofb-prefooter/sofb-prefooter.component */ "./components/layout/sofb-prefooter/sofb-prefooter.component.jsx");







/* harmony default export */ __webpack_exports__["default"] = (function (_ref) {
  var children = _ref.children;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_meta_meta__WEBPACK_IMPORTED_MODULE_1__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_sofb_navbar_sofb_navbar_component__WEBPACK_IMPORTED_MODULE_2__["default"], null), children, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_sofb_banner_sofb_banner_component__WEBPACK_IMPORTED_MODULE_5__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_sofb_prefooter_sofb_prefooter_component__WEBPACK_IMPORTED_MODULE_6__["default"], null), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_sofb_footer_sofb_footer_component__WEBPACK_IMPORTED_MODULE_4__["default"], null));
});

/***/ }),

/***/ "./components/layout/sofb-banner/sofb-banner.component.jsx":
/*!*****************************************************************!*\
  !*** ./components/layout/sofb-banner/sofb-banner.component.jsx ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../constants/sofb.constants */ "./constants/sofb.constants.js");
/* harmony import */ var _sofb_banner_component_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sofb-banner.component.scss */ "./components/layout/sofb-banner/sofb-banner.component.scss");
/* harmony import */ var _sofb_banner_component_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sofb_banner_component_scss__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var SofbBannerComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(SofbBannerComponent, _Component);

  function SofbBannerComponent() {
    _classCallCheck(this, SofbBannerComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(SofbBannerComponent).apply(this, arguments));
  }

  _createClass(SofbBannerComponent, [{
    key: "render",
    value: function render() {
      var state = {
        sofbAdSlides: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_2__["SOFB_STATIC"].content.global.banner.sofbAdSlides,
        poloAndBikeSlides: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_2__["SOFB_STATIC"].content.global.banner.poloAndBikeSlides,
        kappsteinSlides: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_2__["SOFB_STATIC"].content.global.banner.kappsteinSlides,
        bombtrackSlides: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_2__["SOFB_STATIC"].content.global.banner.bombtrackSlides
      };
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid sofb-banner"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-xs-12 col-md-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "carousel-sofb-small"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"], {
        interval: 10000
      }, state.sofbAdSlides.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
          key: it.img
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "SOFB slide"
        }));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-xs-12 col-md-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "carousel-sofb-small"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"], {
        interval: 5000
      }, state.poloAndBikeSlides.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
          key: it.img
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "SOFB slide"
        }));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-xs-12 col-md-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "carousel-sofb-small"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"], {
        interval: 7000
      }, state.kappsteinSlides.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
          key: it.img
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "SOFB slide"
        }));
      })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-xs-12 col-md-3"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "carousel-sofb-small"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"], {
        interval: 3000
      }, state.bombtrackSlides.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Carousel"].Item, {
          key: it.img
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.img,
          alt: "SOFB slide"
        }));
      }))))));
    }
  }]);

  return SofbBannerComponent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SofbBannerComponent);

/***/ }),

/***/ "./components/layout/sofb-banner/sofb-banner.component.scss":
/*!******************************************************************!*\
  !*** ./components/layout/sofb-banner/sofb-banner.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/layout/sofb-footer/sofb-footer.component.jsx":
/*!*****************************************************************!*\
  !*** ./components/layout/sofb-footer/sofb-footer.component.jsx ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../constants/sofb.constants */ "./constants/sofb.constants.js");
/* harmony import */ var _sofb_footer_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sofb-footer.component.scss */ "./components/layout/sofb-footer/sofb-footer.component.scss");
/* harmony import */ var _sofb_footer_component_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_sofb_footer_component_scss__WEBPACK_IMPORTED_MODULE_2__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var SofbFooterComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(SofbFooterComponent, _Component);

  function SofbFooterComponent() {
    _classCallCheck(this, SofbFooterComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(SofbFooterComponent).apply(this, arguments));
  }

  _createClass(SofbFooterComponent, [{
    key: "render",
    value: function render() {
      var menu = _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__["SOFB_STATIC"].content.global.menu;
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("footer", {
        className: "container-fluid"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col-md-8 col-md-offset-2"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("ul", {
        className: "list-inline-footer"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("li", null, menu.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: it.state,
          key: it.state
        }, it.title);
      }))))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "col end-fot"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        target: " _blank",
        href: "/",
        className: " text-muted",
        title: " powered by ARF"
      }, "freepowder "), " \xA9 ", react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: " text-muted"
      }, "2018 "), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
        target: " _blank",
        href: "/",
        className: " text-muted",
        title: " Stoked On Fixed Bikes"
      }, "StokedOnFixedBikes"))));
    }
  }]);

  return SofbFooterComponent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SofbFooterComponent);

/***/ }),

/***/ "./components/layout/sofb-footer/sofb-footer.component.scss":
/*!******************************************************************!*\
  !*** ./components/layout/sofb-footer/sofb-footer.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/layout/sofb-navbar/sofb-navbar.component.jsx":
/*!*****************************************************************!*\
  !*** ./components/layout/sofb-navbar/sofb-navbar.component.jsx ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-bootstrap */ "react-bootstrap");
/* harmony import */ var react_bootstrap__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _sofb_navbar_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sofb-navbar.scss */ "./components/layout/sofb-navbar/sofb-navbar.scss");
/* harmony import */ var _sofb_navbar_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_sofb_navbar_scss__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../constants/sofb.constants */ "./constants/sofb.constants.js");






var NavbarWithLinks = function NavbarWithLinks(_ref) {
  var children = _ref.children,
      router = _ref.router;
  var menu = _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_4__["SOFB_STATIC"].content.global.menu;
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("header", null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"], {
    inverse: true,
    fluid: true
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Header, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Navbar"].Brand, null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
    href: "/"
  }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
    src: "../../static/img/sofb-mini.png",
    className: "logo",
    alt: "Stoked On Fixed Bikes Logo"
  })))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["Nav"], {
    pullRight: true
  }, menu.map(function (it) {
    return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(react_bootstrap__WEBPACK_IMPORTED_MODULE_1__["NavItem"], {
      key: it.state,
      href: it.state
    }, it.title);
  }))));
};

/* harmony default export */ __webpack_exports__["default"] = (Object(next_router__WEBPACK_IMPORTED_MODULE_3__["withRouter"])(NavbarWithLinks));

/***/ }),

/***/ "./components/layout/sofb-navbar/sofb-navbar.scss":
/*!********************************************************!*\
  !*** ./components/layout/sofb-navbar/sofb-navbar.scss ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/layout/sofb-prefooter/sofb-prefooter.component.jsx":
/*!***********************************************************************!*\
  !*** ./components/layout/sofb-prefooter/sofb-prefooter.component.jsx ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../../constants/sofb.constants */ "./constants/sofb.constants.js");
/* harmony import */ var _sofb_social_sofb_social_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../sofb-social/sofb-social.component */ "./components/sofb-social/sofb-social.component.jsx");
/* harmony import */ var _sofb_prefooter_component_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./sofb-prefooter.component.scss */ "./components/layout/sofb-prefooter/sofb-prefooter.component.scss");
/* harmony import */ var _sofb_prefooter_component_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_sofb_prefooter_component_scss__WEBPACK_IMPORTED_MODULE_3__);
function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }






var SofbPrefooterComponent =
/*#__PURE__*/
function (_Component) {
  _inherits(SofbPrefooterComponent, _Component);

  function SofbPrefooterComponent() {
    _classCallCheck(this, SofbPrefooterComponent);

    return _possibleConstructorReturn(this, _getPrototypeOf(SofbPrefooterComponent).apply(this, arguments));
  }

  _createClass(SofbPrefooterComponent, [{
    key: "render",
    value: function render() {
      var state = {
        staticContent: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__["SOFB_STATIC"].content.global.prefooter.content,
        social: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__["SOFB_STATIC"].content.global.social
      };
      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container-fluid prefoot-wrap"
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "row"
      }, state.staticContent.map(function (it, i) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "col-12 col-sm-4",
          key: i
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", null, it.title), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h4", {
          className: "p-1",
          dangerouslySetInnerHTML: {
            __html: it.text
          }
        }), i === 0 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_sofb_social_sofb_social_component__WEBPACK_IMPORTED_MODULE_2__["default"], {
          size: 4
        }) : null, i === 1 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: "/magazines/22"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: "p-2"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.image,
          width: "140px",
          alt: "SOFB Latest magazine"
        }))) : null, i === 2 ? react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: "/"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
          className: "p-2"
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("img", {
          src: it.image,
          width: "250px",
          alt: "SOFB Logo"
        }))) : null);
      })));
    }
  }]);

  return SofbPrefooterComponent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SofbPrefooterComponent);

/***/ }),

/***/ "./components/layout/sofb-prefooter/sofb-prefooter.component.scss":
/*!************************************************************************!*\
  !*** ./components/layout/sofb-prefooter/sofb-prefooter.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./components/meta/meta.js":
/*!*********************************!*\
  !*** ./components/meta/meta.js ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/arfs/dev/sofb/sofb-next/components/meta/meta.js";



/* harmony default export */ __webpack_exports__["default"] = (function () {
  return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 4
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
    __source: {
      fileName: _jsxFileName,
      lineNumber: 5
    },
    __self: this
  }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "viewport",
    content: "width=device-width, initial-scale=1",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 6
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    charSet: "utf-8",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 7
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", {
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 8
    },
    __self: this
  }, "Stoked On Fixed Bikes - SOFB - Fixed Gear Bikes Free Online Magazine - Read Fixed Gear News - Read Magazine"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:image",
    content: "./assets/img/og.jpg",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 9
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:description",
    content: "Fixie - London - Magazine - News - SOFB - Fixed Gear Bikes - Free Online Magazine",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 10
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    property: "og:title",
    content: "Stoked On Fixed Bikes Free Online Magazine",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 11
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "author",
    content: "freepowder - aretanafernandez@gmail.com",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
    rel: "shortcut icon",
    href: "static/favicon.ico",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
    rel: "icon",
    type: "image/x-icon",
    href: "static/favicon.ico",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 14
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
    name: "description",
    content: "Fixed Gear Bikes Free Online Magazine - Stoked On Fixed Bikes",
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 15
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
    href: "https://fonts.googleapis.com/css?family=Roboto",
    rel: "stylesheet",
    async: true,
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 16
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
    rel: "stylesheet",
    href: "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css",
    integrity: "sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u",
    crossOrigin: "anonymous",
    async: true,
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 17
    },
    __self: this
  }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("link", {
    href: "https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css",
    rel: "stylesheet",
    integrity: "sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN",
    crossOrigin: "anonymous",
    async: true,
    className: "jsx-3746602426",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 20
    },
    __self: this
  })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    styleId: "3746602426",
    css: "html,body{height:100%;width:100%;font-family:Roboto,sans-serif;font-size:81.25%;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hcmZzL2Rldi9zb2ZiL3NvZmItbmV4dC9jb21wb25lbnRzL21ldGEvbWV0YS5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUF3QjJCLEFBRzJCLFlBQ0YsV0FDb0IsOEJBQ2IsaUJBQ25CIiwiZmlsZSI6Ii9Vc2Vycy9hcmZzL2Rldi9zb2ZiL3NvZmItbmV4dC9jb21wb25lbnRzL21ldGEvbWV0YS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBIZWFkIGZyb20gJ25leHQvaGVhZCdcblxuZXhwb3J0IGRlZmF1bHQgKCkgPT4gKFxuICAgIDxkaXY+XG4gICAgICAgIDxIZWFkPlxuICAgICAgICAgICAgPG1ldGEgbmFtZT1cInZpZXdwb3J0XCIgY29udGVudD1cIndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xXCIgLz5cbiAgICAgICAgICAgIDxtZXRhIGNoYXJTZXQ9XCJ1dGYtOFwiIC8+XG4gICAgICAgICAgICA8dGl0bGU+U3Rva2VkIE9uIEZpeGVkIEJpa2VzIC0gU09GQiAtIEZpeGVkIEdlYXIgQmlrZXMgRnJlZSBPbmxpbmUgTWFnYXppbmUgLSBSZWFkIEZpeGVkIEdlYXIgTmV3cyAtIFJlYWQgTWFnYXppbmU8L3RpdGxlPlxuICAgICAgICAgICAgPG1ldGEgcHJvcGVydHk9XCJvZzppbWFnZVwiIGNvbnRlbnQ9XCIuL2Fzc2V0cy9pbWcvb2cuanBnXCIvPlxuICAgICAgICAgICAgPG1ldGEgcHJvcGVydHk9XCJvZzpkZXNjcmlwdGlvblwiIGNvbnRlbnQ9XCJGaXhpZSAtIExvbmRvbiAtIE1hZ2F6aW5lIC0gTmV3cyAtIFNPRkIgLSBGaXhlZCBHZWFyIEJpa2VzIC0gRnJlZSBPbmxpbmUgTWFnYXppbmVcIi8+XG4gICAgICAgICAgICA8bWV0YSBwcm9wZXJ0eT1cIm9nOnRpdGxlXCIgY29udGVudD1cIlN0b2tlZCBPbiBGaXhlZCBCaWtlcyBGcmVlIE9ubGluZSBNYWdhemluZVwiLz5cbiAgICAgICAgICAgIDxtZXRhIG5hbWU9XCJhdXRob3JcIiBjb250ZW50PVwiZnJlZXBvd2RlciAtIGFyZXRhbmFmZXJuYW5kZXpAZ21haWwuY29tXCIvPlxuICAgICAgICAgICAgPGxpbmsgcmVsPVwic2hvcnRjdXQgaWNvblwiIGhyZWY9XCJzdGF0aWMvZmF2aWNvbi5pY29cIi8+XG4gICAgICAgICAgICAgICAgPGxpbmsgcmVsPVwiaWNvblwiIHR5cGU9XCJpbWFnZS94LWljb25cIiBocmVmPVwic3RhdGljL2Zhdmljb24uaWNvXCIvPlxuICAgICAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cIkZpeGVkIEdlYXIgQmlrZXMgRnJlZSBPbmxpbmUgTWFnYXppbmUgLSBTdG9rZWQgT24gRml4ZWQgQmlrZXNcIi8+XG4gICAgICAgICAgICA8bGluayBocmVmPVwiaHR0cHM6Ly9mb250cy5nb29nbGVhcGlzLmNvbS9jc3M/ZmFtaWx5PVJvYm90b1wiIHJlbD1cInN0eWxlc2hlZXRcIiBhc3luYy8+XG4gICAgICAgICAgICA8bGluayByZWw9XCJzdHlsZXNoZWV0XCIgaHJlZj1cImh0dHBzOi8vbWF4Y2RuLmJvb3RzdHJhcGNkbi5jb20vYm9vdHN0cmFwLzMuMy43L2Nzcy9ib290c3RyYXAubWluLmNzc1wiXG4gICAgICAgICAgICAgICAgICBpbnRlZ3JpdHk9XCJzaGEzODQtQlZZaWlTSUZlSzFkR21KUkFreWN1SEFIUmczMk9tVWN3dzdvbjNSWWRnNFZhK1BtU1Rzei9LNjh2YmRFamg0dVwiXG4gICAgICAgICAgICAgICAgICBjcm9zc09yaWdpbj1cImFub255bW91c1wiIGFzeW5jLz5cbiAgICAgICAgICAgIDxsaW5rIGhyZWY9XCJodHRwczovL3N0YWNrcGF0aC5ib290c3RyYXBjZG4uY29tL2ZvbnQtYXdlc29tZS80LjcuMC9jc3MvZm9udC1hd2Vzb21lLm1pbi5jc3NcIiByZWw9XCJzdHlsZXNoZWV0XCJcbiAgICAgICAgICAgICAgICAgIGludGVncml0eT1cInNoYTM4NC13dmZYcHFwWlpWUUdLNlRBaDVQVmxHT2ZRTkhTb0QyeGJFK1FrUHhDQUZsTkVldm9FSDNTbDBzaWJWY09RVm5OXCJcbiAgICAgICAgICAgICAgICAgIGNyb3NzT3JpZ2luPVwiYW5vbnltb3VzXCIgYXN5bmMgLz5cbiAgICAgICAgPC9IZWFkPlxuXG4gICAgICAgIDxzdHlsZSBqc3ggZ2xvYmFsPntgXG4gICAgICAgICAgIGh0bWwsIGJvZHl7XG4gICAgICAgICAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgICAgICAgICAgd2lkdGg6MTAwJTtcbiAgICAgICAgICAgICAgZm9udC1mYW1pbHk6IFJvYm90byxzYW5zLXNlcmlmO1xuICAgICAgICAgICAgICBmb250LXNpemU6IDgxLjI1JTtcbiAgICAgICAgICAgIH1cbiAgICBgfTwvc3R5bGU+XG4gICAgPC9kaXY+XG4pIl19 */\n/*@ sourceURL=/Users/arfs/dev/sofb/sofb-next/components/meta/meta.js */",
    __self: this
  }));
});

/***/ }),

/***/ "./components/sofb-social/sofb-social.component.jsx":
/*!**********************************************************!*\
  !*** ./components/sofb-social/sofb-social.component.jsx ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../constants/sofb.constants */ "./constants/sofb.constants.js");
/* harmony import */ var _sofb_social_component_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./sofb-social.component.scss */ "./components/sofb-social/sofb-social.component.scss");
/* harmony import */ var _sofb_social_component_scss__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_sofb_social_component_scss__WEBPACK_IMPORTED_MODULE_2__);
var _jsxFileName = "/Users/arfs/dev/sofb/sofb-next/components/sofb-social/sofb-social.component.jsx";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }





var SofbSocialCompoent =
/*#__PURE__*/
function (_Component) {
  _inherits(SofbSocialCompoent, _Component);

  function SofbSocialCompoent(props) {
    var _this;

    _classCallCheck(this, SofbSocialCompoent);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(SofbSocialCompoent).call(this, props));
    _this.state = {
      social: _constants_sofb_constants__WEBPACK_IMPORTED_MODULE_1__["SOFB_STATIC"].content.global.social
    };
    return _this;
  }

  _createClass(SofbSocialCompoent, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 15
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "list-inline",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 16
        },
        __self: this
      }, this.state.social.map(function (it) {
        return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
          className: "social",
          key: it.icon,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 18
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("a", {
          href: it.link,
          target: "_blank",
          rel: "noopener noreferrer",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 19
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
          className: 'fa fa-' + it.icon + ' fa-fw fa-' + _this2.props.size + 'x',
          __source: {
            fileName: _jsxFileName,
            lineNumber: 20
          },
          __self: this
        })));
      })));
    }
  }]);

  return SofbSocialCompoent;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (SofbSocialCompoent);

/***/ }),

/***/ "./components/sofb-social/sofb-social.component.scss":
/*!***********************************************************!*\
  !*** ./components/sofb-social/sofb-social.component.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./constants/sofb.constants.js":
/*!*************************************!*\
  !*** ./constants/sofb.constants.js ***!
  \*************************************/
/*! exports provided: SOFB_STATIC */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SOFB_STATIC", function() { return SOFB_STATIC; });
/**
 * STATIC CONTENT
 * @type {{content: {global: {menu: *[], banner: {sofbAdSlides: *[], poloAndBikeSlides: *[], kappsteinSlides: *[], bombtrackSlides: *[]}, prefooter: {content: *[]}, footer: {body: {text: string, link: string, brand: string}}, social: *[]}, home: {header: string, h1: string, h2: string, p1: string, p2: string, p3: string}, about: {header: {h2: string, h3: string, p: string}, body: {h2: string, p: string, members: *[]}, banner: string}, advertising: {body: {title: string, oldAdverts: *[]}, header: {title: string, sections: *[]}}, gallery: {body: {title: string, slides: *[]}}}}}
 */
var SOFB_STATIC = {
  content: {
    global: {
      menu: [{
        'title': 'Home',
        'state': '/'
      }, {
        'title': 'Magazines',
        'state': '/magazines'
      }, {
        'title': 'Gallery',
        'state': '/gallery'
      }, {
        'title': 'Advertising',
        'state': '/advertising'
      }, {
        'title': 'Links',
        'state': '/links'
      }, {
        'title': 'Videos',
        'state': '/videos'
      }, {
        'title': 'About',
        'state': '/about'
      }, {
        'title': 'Contact',
        'state': '/contact'
      }],
      banner: {
        sofbAdSlides: [{
          'id': '1',
          'type': 'sofb',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': 'static/img/0.png'
        }, {
          'id': '2',
          'type': 'sofb',
          'url': 'https://www.stokedonfixedbikes.com/',
          'img': 'static/img/0w.png'
        }],
        poloAndBikeSlides: [{
          'id': '1',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
        }, {
          'id': '2',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
        }, {
          'id': '3',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
        }, {
          'id': '4',
          'type': 'poloandbike',
          'url': 'https://www.poloandbike.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
        }],
        kappsteinSlides: [{
          'id': '1',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/1.jpg'
        }, {
          'id': '2',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/2.jpg'
        }, {
          'id': '3',
          'type': 'kappstein',
          'url': 'https://www.kappstein.de/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/3.jpg'
        }],
        bombtrackSlides: [{
          'id': '1',
          'type': 'bombtrack',
          'url': 'https://www.bagaboo.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
        }, {
          'id': '2',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
        }, {
          'id': '3',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
        }, {
          'id': '4',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
        }, {
          'id': '5',
          'type': 'bombtrack',
          'url': 'https://www.bombtrack.com/',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/5.jpg'
        }]
      },
      prefooter: {
        content: [{
          title: 'Stoked On Fixed Bikes',
          text: 'Top Fixed Gear photographers, latest specialist gear, the personalities and the subculture and lifestyle of modern day fixed gear riding.',
          image: ''
        }, {
          title: 'Latest Magazine',
          text: '',
          image: 'https://s3.eu-central-1.amazonaws.com/sofbcovers/cover_sofb_22.jpg'
        }, {
          title: 'Support Fixed Gear',
          text: 'It\'s up to you, free </a> or <a class="donate" target="_blank" rel="noopener" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EGAFDK6FTKQ7S"><strong>donate</strong></a>',
          image: 'https://s3.eu-central-1.amazonaws.com/sofblogos/SOFB_black.png'
        }]
      },
      footer: {
        body: {
          text: 'freepowder',
          link: '',
          brand: 'Stoked On Fixed Bikes'
        }
      },
      social: [{
        icon: 'facebook',
        link: 'https://www.facebook.com/stokedonfixedbikesmag?fref=ts'
      }, {
        icon: 'twitter',
        link: 'https://twitter.com/Sofbonlinemag'
      }, {
        icon: 'instagram',
        link: 'https://www.facebook.com/stokedonfixedbikesmag?fref=ts'
      }, {
        icon: 'vimeo-square',
        link: 'http://vimeo.com/groups/sofbonlinemag'
      }, {
        icon: 'pinterest',
        link: 'http://www.pinterest.com/gfalski/stoked-on-fixed-bikes-magazine/'
      }]
    },
    home: {
      header: 'landing.jpg',
      h1: 'Stoked On Fixed Bikes Free Online Magazine',
      h2: 'Founded by photographer and rider Greg Falski in London 2011.',
      p1: '<strong>The Magazine</strong> is covering competitions, crews, product tests reviews, interviews and photos of passionate riders from around the world.',
      p2: 'SOFB Online Magazine features work from the best fixed gear photographers, ' + 'capturing the essence and energy of the sport. The magazine promotes the subculture and lifestyle of modern day fixed gear riding.',
      p3: 'If you are Stoked On Fixed Bikes thats the right place for you! Riders, Comps, Crews, Bike Porn'
    },
    about: {
      header: {
        'h2': 'Founder & Editor',
        'h3': 'Greg Falski',
        'p': 'Greg has been an avid rider of all types of bikes since he was a kid. ' + 'He studied photography in London and combined his passion for bikes and photography ' + 'in his magazine Stoked On Fixed Bikes.He is an established sports photographer, and has shot ' + 'with brands like Vans, Gumball 3000,Ted James Design, Fixed Gear London, Tokyo Fixed, NS Bikes, Charge Bikes, BB 17,Don´t panic magazine, Karpiel Bikes...just to mention a few.'
      },
      body: {
        'h2': 'The Team',
        'p': 'Stoked On Fixed Bikes',
        'members': [{
          'name': 'Greg Falski',
          'avatar': 'https://s3-us-west-2.amazonaws.com/sofb-landing/g.jpg',
          'role': 'Founder',
          'desc': 'Rider, photographer and editor.'
        }, {
          'name': 'Anto But',
          'avatar': 'https://theradavist.com/old/2010/04/13/AntoButLayout-PINP-thumb.jpg',
          'role': 'Collaborator',
          'desc': 'Legendary London fgfs and bmx rider.'
        }, {
          'name': 'Alvaro Retana',
          'avatar': 'https://lh3.googleusercontent.com/-TUMn6BMvN3A/Ula2QZaQP6I/AAAAAAAAARs/hd3iWx_UD1ctTkjhXRuEeUPc0goypAcCQCEw/w140-h140-p/image.jpeg',
          'role': 'Website',
          'desc': 'Javascript full-stack developer.'
        }]
      },
      banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/12.jpg'
    },
    advertising: {
      body: {
        title: 'We already worked with lots of awesome brands, checkout the old website adverts.',
        oldAdverts: [{
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fyxation/fyxation.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/godandfamous/1.gif'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/4.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/5.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/6.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/4.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/5.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/6.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
        }, {
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
        }]
      },
      header: {
        title: 'Please contact us for more detailed information about advertising.',
        sections: [{
          'icon': 'fa-thumbs-up',
          'title': 'Support',
          'desc': 'Support our magazine by advertising with us. SOFB online mag is free, you can support the team <a class="text-gray-dark" target=\'_blank\' href=\'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EGAFDK6FTKQ7S\'><strong>here</strong></a>.'
        }, {
          'icon': 'fa-newspaper-o',
          'title': 'Magazine',
          'desc': 'Place your products, products reviews, adverts, etc in our magazine, different sizes and formats available.'
        }, {
          'icon': 'fa-laptop',
          'title': 'Website',
          'desc': 'Web Banners. Static image or slider*. Minimum 2 months.<br> *<small>Slider (max. of 6 different images/links).</small>'
        }, {
          'icon': 'fa-gift',
          'title': 'Offers',
          'desc': 'We offer different advertising options. You can choose ads in the magazine or/and website.'
        }]
      }
    },
    gallery: {
      body: {
        title: '',
        slides: [{
          'id': '1',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/redi1.jpg'
        }, {
          'id': '2',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mtb1.jpg'
        }, {
          'id': '3',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/prague1.jpg'
        }, {
          'id': '4',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/respoke1.jpg'
        }, {
          'id': '5',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/red1.jpg'
        }, {
          'id': '6',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini1.jpg'
        }, {
          'id': '7',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini2.jpg'
        }, {
          'id': '8',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini3.jpg'
        }, {
          'id': '9',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/lisbon1.jpg'
        }, {
          'id': '10',
          'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/krakow1.jpg'
        }]
      }
    }
  }
};

/***/ }),

/***/ "./pages/gallery.js":
/*!**************************!*\
  !*** ./pages/gallery.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_layout_main_layout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/layout/main.layout */ "./components/layout/main.layout.js");
/* harmony import */ var _components_gallery_gallery_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/gallery/gallery.component */ "./components/gallery/gallery.component.jsx");




var Gallery = function Gallery() {
  return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_layout_main_layout__WEBPACK_IMPORTED_MODULE_1__["default"], null, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_gallery_gallery_component__WEBPACK_IMPORTED_MODULE_2__["default"], null));
};

/* harmony default export */ __webpack_exports__["default"] = (Gallery);

/***/ }),

/***/ 7:
/*!********************************!*\
  !*** multi ./pages/gallery.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/gallery.js */"./pages/gallery.js");


/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-bootstrap":
/*!**********************************!*\
  !*** external "react-bootstrap" ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-bootstrap");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=gallery.js.map