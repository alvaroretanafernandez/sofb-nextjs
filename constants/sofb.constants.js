/**
 * STATIC CONTENT
 * @type {{content: {global: {menu: *[], banner: {sofbAdSlides: *[], poloAndBikeSlides: *[], kappsteinSlides: *[], bombtrackSlides: *[]}, prefooter: {content: *[]}, footer: {body: {text: string, link: string, brand: string}}, social: *[]}, home: {header: string, h1: string, h2: string, p1: string, p2: string, p3: string}, about: {header: {h2: string, h3: string, p: string}, body: {h2: string, p: string, members: *[]}, banner: string}, advertising: {body: {title: string, oldAdverts: *[]}, header: {title: string, sections: *[]}}, gallery: {body: {title: string, slides: *[]}}}}}
 */
export const SOFB_STATIC = {
    content: {
        global: {
            menu: [
                {
                    'title': 'Home',
                    'state': '/'
                },
                {
                    'title': 'Magazines',
                    'state': '/magazines'
                },
                {
                    'title': 'Gallery',
                    'state': '/gallery'
                },
                {
                    'title': 'Advertising',
                    'state': '/advertising'
                },
                {
                    'title': 'Links',
                    'state': '/links'
                },
                {
                    'title': 'Videos',
                    'state': '/videos'
                },
                {
                    'title': 'About',
                    'state': '/about'
                },
                {
                    'title': 'Contact',
                    'state': '/contact'
                }
            ],
            banner: {
                sofbAdSlides: [
                    {
                        'id': '1',
                        'type': 'sofb',
                        'url': 'https://www.stokedonfixedbikes.com/',
                        'img': 'static/img/0.png'
                    },
                    {
                        'id': '2',
                        'type': 'sofb',
                        'url': 'https://www.stokedonfixedbikes.com/',
                        'img': 'static/img/0w.png'
                    }
                ],
                poloAndBikeSlides: [
                    {
                        'id': '1',
                        'type': 'poloandbike',
                        'url': 'https://www.poloandbike.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
                    },

                    {
                        'id': '2',
                        'type': 'poloandbike',
                        'url': 'https://www.poloandbike.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
                    },
                    {
                        'id': '3',
                        'type': 'poloandbike',
                        'url': 'https://www.poloandbike.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
                    },
                    {
                        'id': '4',
                        'type': 'poloandbike',
                        'url': 'https://www.poloandbike.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
                    }
                ],
                kappsteinSlides: [
                    {
                        'id': '1',
                        'type': 'kappstein',
                        'url': 'https://www.kappstein.de/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/1.jpg'
                    },

                    {
                        'id': '2',
                        'type': 'kappstein',
                        'url': 'https://www.kappstein.de/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/2.jpg'
                    },
                    {
                        'id': '3',
                        'type': 'kappstein',
                        'url': 'https://www.kappstein.de/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/kappstein/3.jpg'
                    }
                ],
                bombtrackSlides: [
                    {
                        'id': '1',
                        'type': 'bombtrack',
                        'url': 'https://www.bagaboo.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
                    },

                    {
                        'id': '2',
                        'type': 'bombtrack',
                        'url': 'https://www.bombtrack.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
                    },
                    {
                        'id': '3',
                        'type': 'bombtrack',
                        'url': 'https://www.bombtrack.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
                    },
                    {
                        'id': '4',
                        'type': 'bombtrack',
                        'url': 'https://www.bombtrack.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
                    },
                    {
                        'id': '5',
                        'type': 'bombtrack',
                        'url': 'https://www.bombtrack.com/',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/5.jpg'
                    }
                ],
            },
            prefooter: {
                content: [
                    {
                        title: 'Stoked On Fixed Bikes',
                        text: 'Top Fixed Gear photographers, latest specialist gear, the personalities and the subculture and lifestyle of modern day fixed gear riding.',
                        image: ''
                    },
                    {
                        title: 'Latest Magazine',
                        text: '',
                        image: 'https://s3.eu-central-1.amazonaws.com/sofbcovers/cover_sofb_22.jpg'
                    },
                    {
                        title: 'Support Fixed Gear',
                        text: 'It\'s up to you, free </a> or <a class="donate" target="_blank" rel="noopener" href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EGAFDK6FTKQ7S"><strong>donate</strong></a>',
                        image: 'https://s3.eu-central-1.amazonaws.com/sofblogos/SOFB_black.png'
                    }
                ]
            },
            footer: {
                body: {
                    text: 'freepowder',
                    link: '',
                    brand: 'Stoked On Fixed Bikes'
                }
            },
            social: [
                {
                    icon: 'facebook',
                    link: 'https://www.facebook.com/stokedonfixedbikesmag?fref=ts',
                },
                {
                    icon: 'twitter',
                    link: 'https://twitter.com/Sofbonlinemag',
                },
                {
                    icon: 'instagram',
                    link: 'https://www.facebook.com/stokedonfixedbikesmag?fref=ts',
                },
                {
                    icon: 'vimeo-square',
                    link: 'http://vimeo.com/groups/sofbonlinemag',
                },
                {
                    icon: 'pinterest',
                    link: 'http://www.pinterest.com/gfalski/stoked-on-fixed-bikes-magazine/',
                }
            ]
        },
        home: {
            header: 'landing.jpg',
            h1: 'Stoked On Fixed Bikes Free Online Magazine',
            h2: 'Founded by photographer and rider Greg Falski in London 2011.',
            p1: '<strong>The Magazine</strong> is covering competitions, crews, product tests reviews, interviews and photos of passionate riders from around the world.',
            p2: 'SOFB Online Magazine features work from the best fixed gear photographers, ' +
            'capturing the essence and energy of the sport. The magazine promotes the subculture and lifestyle of modern day fixed gear riding.',
            p3: 'If you are Stoked On Fixed Bikes thats the right place for you! Riders, Comps, Crews, Bike Porn',
        },
        about: {
            header: {
                'h2': 'Founder & Editor',
                'h3': 'Greg Falski',
                'p': 'Greg has been an avid rider of all types of bikes since he was a kid. ' +
                'He studied photography in London and combined his passion for bikes and photography ' +
                'in his magazine Stoked On Fixed Bikes.He is an established sports photographer, and has shot ' +
                'with brands like Vans, Gumball 3000,Ted James Design, Fixed Gear London, Tokyo Fixed, NS Bikes, Charge Bikes, BB 17,Don´t panic magazine, Karpiel Bikes...just to mention a few.'
            },
            body: {
                'h2': 'The Team',
                'p': 'Stoked On Fixed Bikes',
                'members': [{
                    'name': 'Greg Falski',
                    'avatar': 'https://s3-us-west-2.amazonaws.com/sofb-landing/g.jpg',
                    'role': 'Founder',
                    'desc': 'Rider, photographer and editor.'
                }, {
                    'name': 'Anto But',
                    'avatar': 'https://theradavist.com/old/2010/04/13/AntoButLayout-PINP-thumb.jpg',
                    'role': 'Collaborator',
                    'desc': 'Legendary London fgfs and bmx rider.'
                }, {
                    'name': 'Alvaro Retana',
                    'avatar': 'https://lh3.googleusercontent.com/-TUMn6BMvN3A/Ula2QZaQP6I/AAAAAAAAARs/hd3iWx_UD1ctTkjhXRuEeUPc0goypAcCQCEw/w140-h140-p/image.jpeg',
                    'role': 'Website',
                    'desc': 'Javascript full-stack developer.'
                }]
            },
            banner: 'https://s3.eu-central-1.amazonaws.com/sofbheaders/12.jpg'
        },
        advertising: {
            body: {
                title: 'We already worked with lots of awesome brands, checkout the old website adverts.',
                oldAdverts: [
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bagaboo/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/breakbrake17/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fabike/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/factory_five/3.jpg'
                    },

                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/fyxation/fyxation.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/godandfamous/1.gif'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/4.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/5.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/londonbicycleworkshop/6.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/poloandbike/4.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/purefix/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/4.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/5.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/sanmarco/6.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/1.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/2.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/3.jpg'
                    },
                    {
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbadverts/bombtrack/4.jpg'
                    }
                ],
            },
            header: {
                title: 'Please contact us for more detailed information about advertising.',
                sections: [
                    {
                        'icon': 'fa-thumbs-up',
                        'title': 'Support',
                        'desc': 'Support our magazine by advertising with us. SOFB online mag is free, you can support the team <a class="text-gray-dark" target=\'_blank\' href=\'https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EGAFDK6FTKQ7S\'><strong>here</strong></a>.'
                    },
                    {
                        'icon': 'fa-newspaper-o',
                        'title': 'Magazine',
                        'desc': 'Place your products, products reviews, adverts, etc in our magazine, different sizes and formats available.'
                    },
                    {
                        'icon': 'fa-laptop',
                        'title': 'Website',
                        'desc': 'Web Banners. Static image or slider*. Minimum 2 months.<br> *<small>Slider (max. of 6 different images/links).</small>'
                    },
                    {
                        'icon': 'fa-gift',
                        'title': 'Offers',
                        'desc': 'We offer different advertising options. You can choose ads in the magazine or/and website.'
                    }
                ]
            }
        },
        gallery: {
            body: {
                title: '',
                slides: [
                    {
                        'id': '1',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/redi1.jpg'
                    },
                    {
                        'id': '2',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mtb1.jpg'
                    },
                    {
                        'id': '3',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/prague1.jpg'
                    },
                    {
                        'id': '4',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/respoke1.jpg'
                    },
                    {
                        'id': '5',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/red1.jpg'
                    },
                    {
                        'id': '6',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini1.jpg'
                    },
                    {
                        'id': '7',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini2.jpg'
                    },
                    {
                        'id': '8',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/mini3.jpg'
                    },
                    {
                        'id': '9',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/lisbon1.jpg'
                    },
                    {
                        'id': '10',
                        'img': 'https://s3.eu-central-1.amazonaws.com/sofbgallery/krakow1.jpg'
                    }


                ]
            }

        }
    }
};
