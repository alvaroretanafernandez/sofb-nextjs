const express = require('express');
const next = require('next');
const request = require('request');
const rp = require('request-promise');
const port = parseInt(process.env.PORT, 10) || 13101;
const dev = process.env.NODE_ENV !== "production";
const nextApp = next({ dev });
const nextHandle = nextApp.getRequestHandler();
const SOFB_API_URL = 'https://api.stokedonfixedbikes.com/api/';

nextApp.prepare()
    .then(() => {
        const server = express();

        server.get('/', (req, res) => {
            nextApp.render(req, res, '/', {});
        });

        server.get('/magazines', (req, res) => {

            rp(SOFB_API_URL + 'magazines')
                .then(function (data) {
                    nextApp.render(req, res, '/magazines', { magazines: data });
                })
                .catch(function (err) {
                    // Crawling failed...
                });

        });

        server.get('/magazines/:id', (req, res) => {

            rp(SOFB_API_URL + 'magazines/'+ req.params.id)
                .then(function (data) {
                    nextApp.render(req, res, '/magazine', {
                        magazine: data,
                        previous: Number(req.params.id) -1 === 0 ? null : Number(req.params.id) -1,
                        next: Number(req.params.id) + 1 === 23 ? null :Number(req.params.id) +1
                    });
                })
                .catch(function (err) {
                    // Crawling failed...
                });


        });

        //
        server.get('/links', (req, res) => {

            rp(SOFB_API_URL + 'links')
                .then(function (data) {
                    nextApp.render(req, res, '/links', {
                        links: data
                    });
                })
                .catch(function (err) {
                    // Crawling failed...
                });

        });

        server.get('/videos', (req, res) => {

            rp(SOFB_API_URL + 'videos')
                .then(function (data) {
                    nextApp.render(req, res, '/videos', {
                        videos: data
                    });
                })
                .catch(function (err) {
                    // Crawling failed...
                });

        });

        server.get('/gallery', (req, res) => {
            nextApp.render(req, res, '/gallery', {});
        });

        server.get('/about', (req, res) => {
            nextApp.render(req, res, '/about', {});
        });

        server.get('/advertising', (req, res) => {
            nextApp.render(req, res, '/advertising', {});
        });

        //
        server.get('/contact', (req, res) => {
            nextApp.render(req, res, '/contact', {});
        });

        server.get('*', (req, res) => {
            return nextHandle(req, res)
        });

        server.listen(port, (err) => {
            if (err) {
                throw err;
            }
            console.log(`> Ready on http://localhost:${port}`)
        });
    });
