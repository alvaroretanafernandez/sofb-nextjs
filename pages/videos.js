import React from 'react'
import Layout from '../components/layout/main.layout'
import VideosComponent from '../components/videos/videos.component'

export default class extends React.Component {

    static getInitialProps(ctx) {
        return {
            videos: JSON.parse(ctx.query.videos),
        };
    }
    render() {
        const { videos } = this.props;
        return (
            <Layout>
                <VideosComponent videos={videos}/>
            </Layout>
        )
    }

}