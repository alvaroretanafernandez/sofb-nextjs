import React from 'react'
import Layout from '../components/layout/main.layout'
import Home from '../components/home/home.component'

const Index = () => (
    <Layout>
        <Home/>
    </Layout>
)

export default Index