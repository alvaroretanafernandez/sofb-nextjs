import React from 'react'
import Layout from '../components/layout/main.layout'
import MagazineContent from '../components/magazines/detail/magazine-detail.component'

export default class extends React.Component {
    static getInitialProps(ctx) {
        return {
            magazine: JSON.parse(ctx.query.magazine),
            next: ctx.query.next,
            previous: ctx.query.previous,
        };
    }
    render() {
        const { magazine, next , previous } = this.props;
        return (
            <Layout>
                <MagazineContent magazine={magazine} next={next} previous={previous}/>
            </Layout>
        )
    }
}
