import React from 'react'
import Layout from '../components/layout/main.layout'
import AdvertComponent from '../components/advertising/advertising.component'

const Advertising = () => (
    <Layout>
        <AdvertComponent/>
    </Layout>
)

export default Advertising