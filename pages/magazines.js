import React from 'react'
import Layout from '../components/layout/main.layout'
import MagazinesContent from '../components/magazines/magazines.component'



export default class extends React.Component {

    static getInitialProps(ctx) {

        return {
            magazines: ctx.query.magazines,
        };
    }
    render() {
        const { magazines } = this.props;
        return (
            <Layout>
                <MagazinesContent magazines={magazines ? JSON.parse(magazines) :[] }/>
            </Layout>
        )
    }

}
