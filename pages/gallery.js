import React from 'react'
import Layout from '../components/layout/main.layout'
import GalleryComponent from '../components/gallery/gallery.component'

const Gallery = () => (
    <Layout>
        <GalleryComponent/>
    </Layout>
)

export default Gallery