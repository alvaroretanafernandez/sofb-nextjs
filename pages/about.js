import React from 'react'
import AboutContent from '../components/about/about.component'
import Layout from '../components/layout/main.layout'
const About = () => (
    <Layout>
        <AboutContent/>
    </Layout>
)

export default About