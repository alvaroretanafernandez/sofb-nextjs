import React from 'react'
import Layout from '../components/layout/main.layout'
import ContactComponent from '../components/contact/contact.component'

const Contact = () => (
    <Layout>
        <ContactComponent/>
    </Layout>
)

export default Contact