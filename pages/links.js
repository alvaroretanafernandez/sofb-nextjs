import React from 'react'
import Layout from '../components/layout/main.layout'
import LinksComponent from '../components/links/links.component'

export default class extends React.Component {

    static getInitialProps(ctx) {
        return {
            links: JSON.parse(ctx.query.links)
        };
    }
    render() {
        const { links } = this.props;
        return (
            <Layout>
                <LinksComponent links={links}/>
            </Layout>
        )
    }

}